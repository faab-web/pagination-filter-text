import { PaginationFilterTextModuleOptions } from './../interface/pagination-filter-text-module-options';
import { InjectionToken } from '@angular/core';

export const PAGINATION_FILTER_TEXT_OPTIONS_TOKEN =
new InjectionToken<PaginationFilterTextModuleOptions>('PAGINATION_FILTER_TEXT_OPTIONS_TOKEN');
