import { PaginationEventData } from '@faab/pagination-lib';

export interface PaginationEventTextData extends PaginationEventData {
  value: string;
}
