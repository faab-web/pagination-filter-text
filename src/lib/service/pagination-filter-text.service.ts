import { PaginationFilterTextModuleOptions } from './../interface/pagination-filter-text-module-options';
import { PAGINATION_FILTER_TEXT_OPTIONS_TOKEN } from './../const/pagination-filter-text-options-token';
import { Injectable, Inject } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PaginationFilterTextService {

  private delaySearchField: number = 300;

  constructor(
    @Inject(PAGINATION_FILTER_TEXT_OPTIONS_TOKEN) private token_config: PaginationFilterTextModuleOptions,
  ) {
    const config_default: PaginationFilterTextModuleOptions = {
      delaySearchField: 300
    };
    if (token_config && !(isNaN(token_config.delaySearchField))) {
      this.delaySearchField = token_config.delaySearchField;
    }
  }

  getDelaySearchField(): number {
    return this.delaySearchField;
  }
}
