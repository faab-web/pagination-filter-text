import {
  PaginationTheadCellBaseComponent,
  PaginationEvent,
  PaginationLibComponent,
  PaginationEventData,
  PaginationProcessData,
  PaginationParamItem
} from '@faab/pagination-lib';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Subscription, fromEvent } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { PaginationFilterTextService } from '../service/pagination-filter-text.service';
import { PaginationEventTextData } from '../interface/pagination-event-text-data';
import { isArray, isString } from 'util';

export interface PaginationFilterTextEventValue {
  key: string;
  value: string;
}

@Component({
  selector: 'lib-pagination-filter-text',
  templateUrl: './pagination-filter-text.component.html',
  styles: ['./pagination-filter-text.component.scss']
})
export class PaginationFilterTextComponent extends PaginationTheadCellBaseComponent {

  static EVENT_TYPE = 'TEXT';
  static EVENT_VALUE: PaginationFilterTextEventValue[] = [];
  static PARAM_NAME: string[] = ['filter'];

  private _subscription_key_event_change: Subscription;
  private _subscription_key_event_keyup: Subscription;

  @Input() value: any = '';
  @ViewChild('inputField') inputField: any;

  constructor(
    private paginationFilterTextService: PaginationFilterTextService
  ) {
    super();
  }

  public static updateEventData(event_data: PaginationEventData, process_data?: PaginationProcessData): number {
    // debugger;
    if (
      !event_data
      || !event_data.key
      || !isString(event_data.key)
      || !isString(event_data['value'])
    ) {
      return 1;
    }

    const new_filter_collection: PaginationFilterTextEventValue[] = [],
      data: PaginationEventTextData = {
        key: event_data.key,
        value: event_data['value']
      } as PaginationEventTextData,
      filter_collection_items: PaginationFilterTextEventValue[] =  isArray(PaginationFilterTextComponent.EVENT_VALUE) ?
      PaginationFilterTextComponent.EVENT_VALUE : [];
    let is_new: boolean = true;

    // debugger;

    // [{key: "name", value: "Igor"}]
    filter_collection_items.forEach(
      // {key: "name", value: "Igor"}
      (filter_collection_item: PaginationFilterTextEventValue) => {
        if (isString(filter_collection_item.key)) {
          if (filter_collection_item.key === data.key) {
            filter_collection_item.value = data.value;
            is_new = false;
          }
          if (filter_collection_item.value) {
            new_filter_collection.push(filter_collection_item);
          }
        }
      }
    );
    if (is_new) {
      // debugger;
      new_filter_collection.push({
        key: data.key,
        value: data.value
      } as PaginationFilterTextEventValue);
    }

    // debugger;

    PaginationFilterTextComponent.EVENT_VALUE = new_filter_collection.length > 0 ?
      new_filter_collection : [];
    return 1;
  }

  // param_item = {name: "filter", value: "title=tt,description=d"}
  public static updateEventDataByParamItem(param_item: PaginationParamItem): void {
    // debugger;
    const param_name_arr: string[] = isArray(PaginationFilterTextComponent.PARAM_NAME)
    ? PaginationFilterTextComponent.PARAM_NAME : [];
    if (
      !param_item
      || param_name_arr.indexOf(param_item.name) < 0
      || typeof param_item.value !== 'string'
    ) {
      return;
    }
    const param_values: string[] = param_item.value.split(PaginationLibComponent.SEPARATOR_MANY_VALUE);
    param_values.forEach(
        // "name=Igor"
        (param_item_value_string: string) => {
          const param_item_value_array: string[] = param_item_value_string.split(PaginationLibComponent.SEPARATOR_FILTER_COLLECTION_ITEM);
          if (param_item_value_array[1]) {
            PaginationFilterTextComponent.updateEventData({
              key: param_item_value_array[0],
              value: param_item_value_array[1]
            } as PaginationEventData);
          }
          // self.genFilterField(value);
        }
      );
  }

  public static getParamItem(): string[] {

    const event_values: PaginationFilterTextEventValue[] = isArray(PaginationFilterTextComponent.EVENT_VALUE) ?
      PaginationFilterTextComponent.EVENT_VALUE : [],
      event_values_length: number = event_values.length,
      event_values_arr: string[] = [],
      param_arr: string[] = [];

    for (let i = 0; i < event_values_length; i++) {
      const event_value: PaginationFilterTextEventValue = event_values[i];
      if (event_value.value) {
        event_values_arr.push(event_value.key + PaginationLibComponent.SEPARATOR_FILTER_COLLECTION_ITEM + event_value.value);
      }
    }

    return (
      isArray(event_values_arr)
      && event_values_arr.length > 0
      && isArray(PaginationFilterTextComponent.PARAM_NAME)
      && isString(PaginationFilterTextComponent.PARAM_NAME[0])
    ) ? [PaginationFilterTextComponent.PARAM_NAME[0]
      + PaginationLibComponent.SEPARATOR_VALUE + event_values_arr.join(PaginationLibComponent.SEPARATOR_MANY_VALUE)] : [];
  }

  public static generateRequestUrl(): string {
    const filter_collection_items: PaginationFilterTextEventValue[] = isArray(PaginationFilterTextComponent.EVENT_VALUE) ?
      PaginationFilterTextComponent.EVENT_VALUE : [];
      // filter_collection_items: string[] = filter_collection.split(PaginationLibComponent.SEPARATOR_MANY_VALUE);
    let url: string = '';
    filter_collection_items.forEach(
      (filter_collection_item: PaginationFilterTextEventValue) => {
        if (filter_collection_item.value) {
          url += '&' + PaginationFilterTextComponent.PARAM_NAME[0] + PaginationLibComponent.SEPARATOR_VALUE + filter_collection_item.key
            + PaginationLibComponent.SEPARATOR_FILTER_COLLECTION_ITEM + filter_collection_item.value;
        }
      }
    );
    return url;
  }

  public static getValueByKey(key: string): any {
    // console.log('getValueByKey => (key: ' + key + ')');
    if (!key || typeof key !== 'string') {
      return '';
    }
    const filter_collection_items: PaginationFilterTextEventValue[] = isArray(PaginationFilterTextComponent.EVENT_VALUE) ?
      PaginationFilterTextComponent.EVENT_VALUE : [],
      filter_collection_items_length: number = filter_collection_items.length;

    for (let i = 0; i < filter_collection_items_length; i++) {
      const filter_collection_item: PaginationFilterTextEventValue = filter_collection_items[i];
      if (
        !filter_collection_item
        || !filter_collection_item.key
        || filter_collection_item.key !== key
      ) {
        continue;
      }
      return filter_collection_item.value;
    }
    return '';
  }

  onInit(): void {
    super.onInit();
  }

  onDestroy(): void {
    super.onInit();
  }

  afterViewInit(): void {
    super.afterViewInit();
    // this.prevValue = this.value;
    const self = this;

    this.checkSubscription(this._subscription_key_event_change);
    this._subscription_key_event_change = fromEvent(this.inputField.nativeElement, 'change')
      .pipe(
        debounceTime(this.paginationFilterTextService.getDelaySearchField())
      )
      .subscribe(
        (event: KeyboardEvent) => {
          self.onChange(event);
        }
      );

    this.checkSubscription(this._subscription_key_event_keyup);
    this._subscription_key_event_keyup = fromEvent(this.inputField.nativeElement, 'keyup')
      .pipe(
        debounceTime(this.paginationFilterTextService.getDelaySearchField())
      )
      .subscribe(
        (event: KeyboardEvent) => {
          self.onChange(event);
        }
      );
  }

  onChange(e: KeyboardEvent) {
    if (!(e instanceof KeyboardEvent)) {
      return;
    }
    e.stopPropagation();
    e.preventDefault();

    this.value = (e && e.target && typeof e.target['value'] === 'string')
    ? e.target['value'] : '';
    this.emitEvent();
  }

  public clear(): void {
    this.value = '';
    this.emitEvent();
  }

  private emitEvent(): void {
    const event: PaginationEvent = new PaginationEvent(
      PaginationFilterTextComponent.EVENT_TYPE,
      {
        key: this.key,
        value: this.value
      } as PaginationEventTextData
    );
/*     event.type = PaginationFilterTextComponent.EVENT_TYPE;
    event.data = {
      key: this.key,
      value: this.value
    } as PaginationEventTextData; */
    this.changePaginationEvent.emit(event);
  }

  public debugEventValue(): any {
    return PaginationFilterTextComponent.EVENT_VALUE;
  }
}

/* @StaticImplements<PaginationTheadCellStaticInterface>()
export class PaginationTheadCellBaseComponent implements PaginationTheadCellComponentInterface */
