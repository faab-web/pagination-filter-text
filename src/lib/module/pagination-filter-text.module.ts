import { PAGINATION_FILTER_TEXT_OPTIONS_TOKEN } from './../const/pagination-filter-text-options-token';
import { PaginationFilterTextModuleOptions } from './../interface/pagination-filter-text-module-options';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PaginationFilterTextComponent } from '../component/pagination-filter-text.component';
import { PaginationLibModule } from '@faab/pagination-lib';
import { ModuleWithProviders } from '@angular/compiler/src/core';

@NgModule({
  declarations: [
    PaginationFilterTextComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    PaginationLibModule
  ],
  entryComponents: [
    PaginationFilterTextComponent
  ],
  exports: [
    PaginationFilterTextComponent
  ]
})
export class PaginationFilterTextModule {
  static forRoot(options?: PaginationFilterTextModuleOptions): ModuleWithProviders {
    return {
      ngModule: PaginationFilterTextModule,
      providers: [
        {
          provide: PAGINATION_FILTER_TEXT_OPTIONS_TOKEN,
          useValue: options,
        }
      ]
    };
  }
}
